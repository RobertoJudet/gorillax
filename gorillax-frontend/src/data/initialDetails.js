const initialDetails = [
    {
      id: 1,
      imgPath: "/assets/img/ARTIST.svg",
      name: "Jane Doe",
      email: "janedoe@gmail.com",
      address: "New Delhi, India",
    },
    {
      id: 2,
      imgPath: "/assets/img/ASTRONAUT.svg",
      name: "Mary Rosamund",
      email: "agra@rosie.com",
      address: "Tbilisi, India",
    },
    {
      id: 3,
      imgPath: "/assets/img/CHEF.svg",
      name: "Sherlock Watson",
      email: "irene@johnlock.com",
      address: "Baker Street, India",
    },
    {
      id: 4,
      imgPath: "/assets/img/DIVER.svg",
      name: "John Holmes",
      email: "mary@johnlock.com",
      address: "Baker Street, India",
    },
    {
      id: 5,
      imgPath: "/assets/img/NURSE.svg",
      name: "Mycroft Lestrade",
      email: "britishgovt@gmail.com",
      address: "London, India",
    },
  ];
  
  export default initialDetails;