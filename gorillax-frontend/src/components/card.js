import React from 'react';

function Card({enty, handleClick}) {
  function handleCardClick() {
    handleClick(enty)
  }
  console.log(enty)
  if(enty.cover_url == "url"){
    enty.cover_url = "https://www.syracuse.com/resizer/Cj7sp4D9A2b_4IW--YiyJgJKUiQ=/1280x0/smart/cloudfront-us-east-1.images.arcpublishing.com/advancelocal/Z4JXG2AZEZB55IGXLKTBGGXTDQ.jpeg"
  }
  return(
    <div onClick={handleCardClick} className="tc bg-light-green dib br3 pa3 ma2 grow bw2 shadow-5" style={{ width: '300px', backgroundColor: "#FDF249" }}>
      <img className="br-100 h3 w3 dib" alt={enty.title} src={enty.cover_url} />
      <div>
        <h2 style={{ flexWrap:"wrap" }}>{enty.title}</h2>
        <p>{enty.type}</p>
        <p>{enty.year}</p>
      </div>
    </div>
  );
}

export default Card;