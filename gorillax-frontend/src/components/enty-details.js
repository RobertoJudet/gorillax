
import React, { useState, useEffect, CSSProperties } from 'react';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Scroll from './scroll';
import SearchList from './search_list';
import Button from '@mui/material/Button';
import ClipLoader from "react-spinners/ClipLoader";

const override = {
    display: "block",
    margin: "0 auto",
    borderColor: "red",
};


function EntyDetails({enty, closeModal}) {
    const [recommendations, setRecommendations]=useState([]);
    useEffect(() => getRecommendations, []);

    let [loading, setLoading] = useState(true);
    let [color, setColor] = useState("#ecfc03");

    function getRecommendations() {
        const token = localStorage.getItem('token');
        fetch(`http://127.0.0.1:8000/enty/recommendations?title=${enty.title}`, {
          method: 'get',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          },
        })
        .then(response => response.json())
        .then(data => {
          setRecommendations(data)
          setLoading(false)
        })
    }

    function removeEnty() {
        const token = localStorage.getItem('token');
        fetch(`http://127.0.0.1:8000/enty?enty_id=${enty.id}`, {
          method: 'delete',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          },
        })
        .then(response => response.json())
        .then(data => {
         closeModal()
        })
      }

    function toggleEntyWatched(){
        const token = localStorage.getItem('token');
        fetch(`http://127.0.0.1:8000/enty?enty_id=${enty.id}`, {
          method: 'PATCH',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          },
        })
        .then(() => {
         closeModal()
        })
    }

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    let recommendationsElements = [
        <div>No recommendation</div>
    ]
    if(recommendations){
        recommendationsElements = recommendations.map(recommendation =>  <div>&#x2022; {capitalizeFirstLetter(recommendation["Name"])}</div>); 
    }

    let wtc = []

    if(enty.watched == true){
        if(enty.type == "movie" || enty.type == "tv series"){
            wtc = [
                <div>watched</div>,
            ]
        } else if(enty.type == "book") {
            wtc = [
                <div>read</div>,
            ]
        }
    } else {
        if(enty.type == "movie" || enty.type == "tv serie"){
            wtc = [
                <div>not watched</div>,
                <Button variant="contained" onClick={toggleEntyWatched}>Mark as watched</Button>
            ]
        } else if(enty.type == "book") {
            wtc = [
                <div>not read</div>,
                <Button variant="contained" onClick={toggleEntyWatched}>Mark as read</Button>
            ]
        }
    }


    return(
      <div style={{display: "inline-flex", position: "relative", width: "100%"}}>
        <div style={{width: "50%"}}>
            <img className="br-100 h3 w3 dib" alt={enty.title} src={enty.cover_url} />
            <div>
            <h2 style={{ flexWrap:"wrap" }}>{enty.title}</h2>
            <p>{enty.type}</p>
            <p>Year: {enty.year}</p>
            {wtc}
            </div>
            <Button variant="contained" color="error" onClick={removeEnty}>Remove</Button>

        </div>
        <div style={{width: "50%", height: "50%", position: "relative"}}>
          <img src="https://img.icons8.com/material-outlined/192/share.png" width="40" style={{ float: 'right' }}/>
            <div>Recommendations:</div>
            <div><ClipLoader color={color} loading={loading} cssOverride={{display: "table", margin: "25% auto 0"}} size={50}/></div>
            {recommendationsElements}
            <div>Available on: {enty.availability}</div>
            <div>
            </div>
        </div>
      </div>
    );
  }
  
  export default EntyDetails;