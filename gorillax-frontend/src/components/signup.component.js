import React, { Component } from 'react'
import { useNavigate } from 'react-router-dom';
import {withRouter} from './withRouter';

class SignUp extends Component {
  constructor(props){
   super(props);
   this.handleSubmit = this.handleSubmit.bind(this);
   this.navigateToSignIn=this.navigateToSignIn.bind(this);
  }
   navigateToSignIn()
    {
        this.props.navigate('/sign-in')
    }
  handleSubmit(event){
    let body = {
        "first_name": this.firstName.value,
        "last_name": this.lastName.value,
        "email": this.email.value,
        "password": this.password.value
       }
    event.preventDefault();
    fetch("http://127.0.0.1:8000/users", {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    })
    .then(response => response.json())
    .then(data => {
       this.navigateToSignIn()
    });
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <h3>Sign Up</h3>
        <div className="mb-3">
          <label>First name</label>
          <input
            ref={(ref) => {this.firstName = ref}}
            name="first_name"
            type="text"
            className="form-control"
            placeholder="First name"
          />
        </div>
        <div className="mb-3">
          <label>Last name</label>
          <input
            ref={(ref) => {this.lastName = ref}}
            name="last_name"
            type="text"
            className="form-control"
            placeholder="Last name"
           />
        </div>
        <div className="mb-3">
          <label>Email address</label>
          <input
            ref={(ref) => {this.email = ref}}
            name="email"
            type="email"
            className="form-control"
            placeholder="Enter email"
          />
        </div>
        <div className="mb-3">
          <label>Password</label>
          <input
            ref={(ref) => {this.password = ref}}
            name="password"
            type="password"
            className="form-control"
            placeholder="Enter password"
          />
        </div>
        <div className="d-grid">
          <button type="submit" className="btn btn-primary">
            Sign Up
          </button>
        </div>
        <p className="forgot-password text-right">
          Already registered <a href="/sign-in">sign in?</a>
        </p>
      </form>
    )
  }
}

export default withRouter(SignUp)