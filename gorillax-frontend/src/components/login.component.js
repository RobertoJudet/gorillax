import React, { Component } from 'react'
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom'
import {withRouter} from './withRouter';

class Login extends Component {
  constructor(props){
   super(props);
   this.handleSubmit = this.handleSubmit.bind(this);
   this.navigateToHome=this.navigateToHome.bind(this);
  }

  navigateToHome()
    {
        this.props.navigate('/home')
    }

  handleSubmit(event){
    let body = {
        "email": this.email.value,
        "password": this.password.value
       }
    event.preventDefault();
    fetch("http://127.0.0.1:8000/users/login", {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    })
    .then(response => response.json())
    .then(data => {
        localStorage.setItem('token', data["jwt_token"]);
        this.navigateToHome()
    })
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <h3>Sign In</h3>
        <div className="mb-3">
          <label>Email address</label>
          <input
            ref={(ref) => {this.email = ref}}
            name="email"
            type="email"
            className="form-control"
            placeholder="Enter email"
          />
        </div>
        <div className="mb-3">
          <label>Password</label>
          <input
            ref={(ref) => {this.password = ref}}
            name="password"
            type="password"
            className="form-control"
            placeholder="Enter password"
          />
        </div>
        <div className="mb-3">
          <div className="custom-control custom-checkbox">
            <input
              type="checkbox"
              className="custom-control-input"
              id="customCheck1"
            />
            <label className="custom-control-label" htmlFor="customCheck1">
              Remember me
            </label>
          </div>
        </div>
        <div className="d-grid">
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </div>
        <p className="forgot-password text-right">
          Forgot <a href="#">password?</a>
        </p>
        <p> 
            <Link className="nav-link" to={'/sign-up'}>
                Don't have an account? Sign up
            </Link>
        </p>
      </form>
    )
  }
}

export default withRouter(Login)