import React, { useState, useEffect } from 'react';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Scroll from './scroll';
import SearchList from './search_list';
import EntyDetails from './enty-details';

function Search() {

  const [searchField, setSearchField] = useState("");
  const [details, setDetails]=useState([]);
  const [searchedEnties, setSearchedEnties]=useState([]);

  const [searchTerm, setSearchTerm] = useState('')

  const [userInfo, setUserInfo] = useState({})


  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);


  const [openDetails, setOpenDetails] = useState(false);
  const handleOpenDetails = () => setOpenDetails(true);
  const handleCloseDetails = () => {setOpenDetails(false); setEnties()}

  const [activeEnty, setActiveEnty]=useState({})


  useEffect(() => setEnties, []);
  useEffect(() => searchEnties, []);
  useEffect(() => getUserInfo, {});

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };

  
  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      console.log(searchTerm)
      searchEnties(searchTerm)
    }, 500)

    return () => clearTimeout(delayDebounceFn)
  }, [searchTerm])
  

  function getUserInfo() {
   const token = localStorage.getItem('token');
   fetch("http://127.0.0.1:8000/users/me", {
      method: 'get',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    })
    .then(response => response.json())
    .then(data => {
      setUserInfo(data)
      console.log(">>>>>>>>>>>>>>>>>>>>>>>>>")
      console.log(data)
    })
  }
  
  function setEnties()  {
   const token = localStorage.getItem('token');
   fetch("http://127.0.0.1:8000/enty", {
      method: 'get',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    })
    .then(response => response.json())
    .then(data => {
        setDetails(data)
    })
  }

  function searchEnties(name) {
        if(name) {
        const token = localStorage.getItem('token');
        fetch(`http://127.0.0.1:8000/enty/search?title=${name}`, {
          method: 'get',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          }
        })
        .then(response => response.json())
        .then(data => {
          console.log(data)
          setSearchedEnties(data)
        })
      }
  }

  const filteredPersons = details.filter(
    enty => {
      return (
        enty
        .title
        .toLowerCase()
        .includes(searchField.toLowerCase())
//        enty
//        .email
//        .toLowerCase()
//        .includes(searchField.toLowerCase())
      );
    }
  );

  const handleChange = e => {
    setSearchField(e.target.value);
  };

  function searchList() {
    return (
      <Scroll>
        <SearchList filteredPersons={filteredPersons} handleClick={openEntyDetails}/>
      </Scroll>
    );
  }


  function handleCardClick(enty) {
    if(enty.type == "book" || enty.type == "video game"){
      enty["tmdb_id"] = 0
    }
    enty["availability"]=""
    const token = localStorage.getItem('token');
    fetch("http://127.0.0.1:8000/enty", {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(enty)
    })
    .then(response => response.json())
    .then(data => {
      handleClose()
      setEnties()
    })
  }

  function searchedEntyList() {
    return (
      <Scroll>
        <SearchList filteredPersons={searchedEnties} handleClick={handleCardClick} />
      </Scroll>
    );
  }


  function addEnty() {
    setSearchedEnties([])
    handleOpen()
  }

  function openEntyDetails(enty) {
    setActiveEnty(enty)
    handleOpenDetails()
  }

  return (
    <section className="garamond">
      <div className="navy georgia ma0 grow">
        <h2 className="f2"></h2>
      </div>
      <div className="pa2">
        <h2>Planted trees: {userInfo.planted_trees}</h2>
        <input 
          className="pa3 bb br3 grow b--none bg-lightest-blue ma3"
          type = "search"
          placeholder = "Search your saved stuff" 
          onChange = {handleChange}
        />
        <input 
          className="pa3 bb br3 grow b--none bg-lightest-blue ma3"
          type = "button"
          value = "+" 
          onClick = {addEnty}
        />
      </div>
      <div>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <div>
              <input 
                className="pa3 bb br3 grow b--none bg-lightest-blue ma3"
                type = "search"
                placeholder = "Search stuff" 
                onChange = {(e) => setSearchTerm(e.target.value)}
              />
            </div>
            {searchedEntyList()}
          </Box>
        </Modal>
      </div>
      <div>
        <Modal
          open={openDetails}
          onClose={handleCloseDetails}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <EntyDetails enty={activeEnty} closeModal={handleCloseDetails}/>
          </Box>
        </Modal>
      </div>
      {searchList()}
    </section>
  );
}

export default Search;