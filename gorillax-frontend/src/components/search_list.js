
import React from 'react';
import Card from './card';

function SearchList({ filteredPersons, handleClick}) {
  const filtered = filteredPersons.map(enty =>  <Card key={enty.id} enty={enty} handleClick={handleClick}/>); 
  return (
    <div style={{ display: "flex", flexWrap:"wrap" }}>
      {filtered}
    </div>
  );
}

export default SearchList;
