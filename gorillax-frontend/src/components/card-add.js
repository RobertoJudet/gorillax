import React from 'react';

function CardAdd({enty}) {
  return(
    <div className="tc bg-light-green dib br3 pa3 ma2 grow bw2 shadow-5">
      <img className="br-100 h3 w3 dib" alt={enty.title} src={enty.cover_url} />
      <div>
        <h2>{enty.title}</h2>
        <p>{enty.type}</p>
        <p>{enty.year}</p>
      </div>
    </div>
  );
}

export default CardAdd;