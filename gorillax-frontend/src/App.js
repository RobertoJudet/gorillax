import React from 'react'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './App.css'
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom'
import Login from './components/login.component'
import SignUp from './components/signup.component'
import Search from './components/search'

function App() {
  return (
    <Router>
      <div className="App" style={{ backgroundColor: "#f9fafb" }}>
        <nav>
          <div className="container">
            <img src="gorillax-logo.png" width="200" style={{ paddingTop: "10px", paddingBottom: "40px" }}/>
          </div>
        </nav>
        <div className="auth-wrapper">
          <div className="auth-inner">
            <Routes>
              <Route exact path="/" element={<Login />} />
              <Route path="/sign-in" element={<Login />} />
              <Route path="/sign-up" element={<SignUp />} />
              <Route path="/home" element={<Search/>} />
            </Routes>
          </div>
        </div>
      </div>
    </Router>
  )
}
export default App