import jwt
import bcrypt
from fastapi import APIRouter, Depends, Request

from sqlalchemy.future import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import get_session
from app.models.users import User
from app.models.auth import Auth, AuthRead, Login
from app.auth.crypto import salt, hash_pass

router = APIRouter(prefix="/users", tags=["users"])

my_secret = "my_super_secret"


@router.post("/login", response_model=AuthRead)
async def login(
    request: Request, login_payload: Login, session: AsyncSession = Depends(get_session)
):
    user = await request.app.mongodb["users"].find_one({"email": login_payload.email})
    # result = await session.execute(select(User).where(User.email == login_payload.email))
    # user = result.scalars().first()
    hashed_password = hash_pass(login_payload.password).decode("utf-8")
    print(login_payload)
    print(hashed_password)
    if hashed_password == user["password"]:
        payload_data = {
            "sub": str(user["_id"]),
            "first_name": user["first_name"],
            "last_name": user["last_name"],
        }
        token = jwt.encode(payload=payload_data, key=my_secret)
        auth = Auth(id=str(user["_id"]), user_id=str(user["_id"]), jwt_token=token)
        db_auth = Auth.from_orm(auth)
        print("db model")
        print(db_auth)
        session.add(db_auth)
        await session.commit()
        await session.refresh(db_auth)
        return db_auth
