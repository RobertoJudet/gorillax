import requests
import uuid
from fastapi import APIRouter, Depends, Query, HTTPException, Request
from app.auth.current_active_user import get_current_user

from typing import List

from sqlalchemy.future import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.auth.current_active_user import get_current_user
from app.auth.auth_bearer import JWTBearer
from app.db import get_session
from app.models.enties import EntyBase, Enty, EntyTypes
from app.models.users import User
from app.recommendation.recommendation import get_recommendation_results
from bson import ObjectId
from app.auth.auth_bearer import JWTBearer
from imdb import Cinemagoer, IMDbError

router = APIRouter(prefix="/enty", tags=["enty"])


@router.get("", dependencies=[Depends(JWTBearer())], response_model=list[Enty])
async def get_enties(
    request: Request,
    session: AsyncSession = Depends(get_session),
    user: User = Depends(get_current_user),
):
    if not "enties" in user:
        return []
    return [
        Enty(
            id=enty["id"],
            title=enty["title"],
            type=enty["type"],
            year=enty["year"],
            user_id=str(enty["user_id"]),
            cover_url=enty["cover_url"],
            watched=enty["watched"],
            tmdb_id=enty["tmdb_id"],
            availability=enty["availability"] if "availability" in enty else "not available",
        )
        for enty in user["enties"]
    ]


@router.patch("", dependencies=[Depends(JWTBearer())])
async def mark_as_watched(
    request: Request,
    enty_id: int,
    session: AsyncSession = Depends(get_session),
    user: User = Depends(get_current_user),
):
    # result = await session.execute(select(Enty).where(Enty.user_id == user.id).where(Enty.id == enty_id))
    # enty = result.scalars().first()
    if not "enties" in user:
        raise HTTPException(
            status_code=404, detail=f"There's no enty with the specified id: {enty_id}"
        )
    user["enties"][enty_id]["watched"] = True
    # enty["watched"] = True
    # session.add(enty)
    user["planted_trees"] += 1
    # await session.commit()
    # await session.refresh(enty)
    new_task = await request.app.mongodb["users"].update_one(
        {"_id": user["_id"]},
        {"$set": {"enties": user["enties"], "planted_trees": user["planted_trees"]}},
    )


@router.post("", response_model=Enty)
async def add_enty(
    request: Request,
    enty: EntyBase,
    session: AsyncSession = Depends(get_session),
    user: User = Depends(get_current_user),
):
    if not "enties" in user:
        user["enties"] = []
    availability = await check_availability(enty.tmdb_id)
    new_enty = {
        "id": len(user["enties"]),
        "title": enty.title,
        "type": enty.type,
        "year": enty.year,
        "cover_url": enty.cover_url,
        "user_id": str(user["_id"]),
        "watched": False,
        "tmdb_id": enty.tmdb_id,
        "availability": enty.availability,
    }
    # if enty.type == EntyTypes.tv_series:
    #     new_enty["season"] = enty.season
    user["enties"].append(new_enty)
    new_task = await request.app.mongodb["users"].update_one(
        {"_id": user["_id"]}, {"$set": {"enties": user["enties"]}}
    )
    return new_enty


@router.delete("", dependencies=[Depends(JWTBearer())])
async def delete_enty(
    request: Request,
    enty_id: int,
    session: AsyncSession = Depends(get_session),
    user: User = Depends(get_current_user),
):
    if not "enties" in user:
        raise HTTPException(
            status_code=404, detail=f"There's no enty with the specified id: {enty_id}"
        )
    new_enties = user["enties"]
    new_enties.pop(enty_id)
    i = 0
    for enty in new_enties:
        enty["id"] = i
        i += 1
    new_task = await request.app.mongodb["users"].update_one(
        {"_id": user["_id"]}, {"$set": {"enties": new_enties}}
    )
    return {"deleted": True}


@router.get("/search", dependencies=[Depends(JWTBearer())])
async def search(title: str, enty_types: List[str] = Query(None)):
    supported_enty_types = EntyTypes.list()
    results = []

    if enty_types:
        for enty in enty_types:
            if enty not in supported_enty_types:
                raise HTTPException(
                    status_code=400, detail="Error: enty type not supported."
                )
        results = add_enty_to_result(results, title, enty_types)
        if "book" in enty_types:
            results = add_books(results, title)

    if enty_types is None:
        enty_types = EntyTypes.list()
        results = add_enty_to_result(results, title, enty_types)
        results = add_books(results, title)
    return results


# @router.get("/search/tv_show", dependencies=[Depends(JWTBearer())])
# async def get_tv_show_seasons(
#     tv_series_id: int, session: AsyncSession = Depends(get_session)
# ):
#     show = await session.execute(select(TvSeries).where(TvSeries.id == tv_series_id))
#     return show.season


@router.get("/check_availability", dependencies=[Depends(JWTBearer())])
async def check_availability(
    tmdb_id: int,
    country: str = "RO",
    session: AsyncSession = Depends(get_session),
    user: User = Depends(get_current_user),
):
    tmdb_key = "88e5a89f5588fb72a369db393ab9879e"

    # result = await session.execute(
    #     select(Enty).where(Enty.user_id == user.id).where(Enty.id == enty_id)
    # )
    # enty = result.scalars().first()
    # result = await session.execute(select(Enty).where(Enty.user_id == user.id).where(Enty.id == enty_id))
    # enty = result.scalars().first()

    availability = requests.get(
        f"https://api.themoviedb.org/3/movie/{tmdb_id}/watch/providers?api_key={tmdb_key}"
    ).json()

    try:
        return ",".join([platform["provider_name"] for platform in availability["results"][country]["flatrate"]])
    # except:
    #     try:
    #         disponibility = []
    #         for country in availability["results"].keys():
    #             country_disp = {
    #                 country: [platform["provider_name"] for platform in availability["results"][country]["ads"]]}
    #             disponibility.append(country_disp)
    #         return disponibility
    except:
        return "Not in your country"


@router.get("/network", dependencies=[Depends(JWTBearer())])
async def network():
    network_id = "384"
    r = requests.get(
        f"https://api.themoviedb.org/3/network/{network_id}?api_key=88e5a89f5588fb72a369db393ab9879e"
    ).json()

    return r


@router.get("/recommendations", dependencies=[Depends(JWTBearer())])
async def get_recommendations(title: str):
    recommendations = get_recommendation_results(title)
    return recommendations


# def get_episodes(show, season):
#     return [show["episodes"][season]]


def add_books(results, title):
    books_api_key = "AIzaSyBf4PXHmaGLzf40NAcvuSfBPFyQai66oM8"

    params = {
        "q": title,
        "key": books_api_key,
    }

    books = requests.get(
        "https://www.googleapis.com/books/v1/volumes", params=params
    ).json()
    for book in books["items"]:
        try:
            db_book = {
                "title": book["volumeInfo"]["title"],
                "type": "book",
                "year": book["volumeInfo"]["publishedDate"].split("-")[0],
                "cover_url": book["volumeInfo"]["imageLinks"]["thumbnail"],
            }
            results.append(db_book)
        except:
            print(f"Info not provided for {book['volumeInfo']['title']} - Book")

    return results


def add_enty_to_result(results, title, enty_types):
    tmdb_key = "88e5a89f5588fb72a369db393ab9879e"

    if EntyTypes.movie in enty_types:
        results = add_movie_for_tmdb(results, title, tmdb_key, enty_types)
        enty_types.remove(EntyTypes.movie)

    if EntyTypes.tv_series in enty_types:
        results = add_tv_for_tmdb(results, title, tmdb_key, enty_types)
        enty_types.remove(EntyTypes.tv_series)

    if enty_types:
        try:
            ia = Cinemagoer()
            for enty in ia.search_movie(title):
                if enty["kind"] in enty_types:
                    try:
                        db_enty = {
                            "title": enty["title"],
                            "type": enty["kind"],
                            "year": enty["year"] if enty["year"] else "0000",
                            "cover_url": enty["cover url"],
                        }
                        results.append(db_enty)
                    except:
                        print(f"Info not provided for {enty['title']} - {enty['kind']}")
                        continue
        except IMDbError as e:
            print(e)
            raise IMDbError

    return results


def add_movie_for_tmdb(results, title, tmdb_key, enty_types):
    response = requests.get(
        f"https://api.themoviedb.org/3/search/movie?api_key={tmdb_key}&query={title}"
    ).json()
    for result in response["results"]:
        # image = requests.get(f"https://api.themoviedb.org/3/movie/{result['id']}/images?api_key={tmdb_key}").json()

        try:
            db_result = {
                "title": result["title"],
                "type": EntyTypes.movie,
                "year": result["release_date"].split("-")[0],
                "cover_url": "url",
                "tmdb_id": result["id"],
            }
        except:
            print(f"Info not provided for {result['title']} - Movie")
            continue
        results.append(db_result)

    return results


def add_tv_for_tmdb(results, title, tmdb_key, enty_types):
    response = requests.get(
        f"https://api.themoviedb.org/3/search/tv?api_key={tmdb_key}&query={title}"
    ).json()

    for result in response["results"]:
        # image = requests.get(f"https://api.themoviedb.org/3/tv/{tmdb_key}/images?api_key={tmdb_key}").json()

        try:
            db_result = {
                "title": result["name"],
                "type": EntyTypes.tv_series,
                "year": result["first_air_date"].split("-")[0],
                "cover_url": "url",
                "tmdb_id": result["id"],
            }
        except:
            print(f"Info not provided for {result['name']} - TvShow")
            continue
        results.append(db_result)

    return results
