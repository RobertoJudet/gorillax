import bcrypt

from fastapi import APIRouter, Depends, Request

from sqlalchemy.future import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import get_session
from app.models.users import User, UserRead, UserCreate

from app.auth.auth_bearer import JWTBearer
from app.auth.current_active_user import get_current_user
from app.auth.crypto import salt, hash_pass
from fastapi.encoders import jsonable_encoder
import requests

router = APIRouter(prefix="/users", tags=["users"])


@router.get("", dependencies=[Depends(JWTBearer())], response_model=list[UserRead])
async def get_users(request: Request, session: AsyncSession = Depends(get_session)):
    # result = await session.execute(select(User))
    # users = result.scalars().all()
    users = await request.app.mongodb["users"].find().to_list(length=None)
    return [
        UserRead(
            first_name=user["first_name"],
            last_name=user["last_name"],
            email=user["email"],
            id=str(user["_id"]),
            planted_trees=user["planted_trees"],
        )
        for user in users
    ]


@router.get("/me", response_model=UserRead)
async def get_user(request: Request, user: User = Depends(get_current_user)):
    return UserRead(
        first_name=user["first_name"],
        last_name=user["last_name"],
        email=user["email"],
        id=str(user["_id"]),
        planted_trees=user["planted_trees"],
    )


@router.post("")
async def add_user(
    request: Request, user: UserCreate, session: AsyncSession = Depends(get_session)
):
    print(user)
    hashed_password = hash_pass(user.password)
    print(hashed_password)
    user = UserCreate(
        first_name=user.first_name,
        last_name=user.last_name,
        email=user.email,
        password=hashed_password,
        planted_trees=0,
    )
    session_user = User.from_orm(user)
    session.add(session_user)
    mongodb_user = jsonable_encoder(user)
    new_user = await request.app.mongodb["users"].insert_one(mongodb_user)
    created_user = await request.app.mongodb["users"].find_one(
        {"_id": new_user.inserted_id}
    )
    print("created user:")
    print(created_user)
    # await session.commit()
    # await session.refresh(db_user)
    # created_user["_id"] = str(created_user["_id"])
    # return created_user
