import bcrypt

salt = b"$2b$16$kTcOWHPe2HsMqN.vw/KlDO"



def hash_pass(password):
    return bcrypt.hashpw(password.encode("utf-8"), salt)
