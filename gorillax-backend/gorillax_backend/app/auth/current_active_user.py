from fastapi import APIRouter, Depends, Request

from app.auth.auth_bearer import JWTBearer

from sqlalchemy.future import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import get_session
from app.models.users import User, UserRead
from bson import ObjectId


async def get_current_user(
    request: Request,
    decoded_jwt: dict = Depends(JWTBearer()),
    session: AsyncSession = Depends(get_session),
):
    print("current user:")
    print(decoded_jwt["sub"])
    user = await request.app.mongodb["users"].find_one(
        {"_id": ObjectId(decoded_jwt["sub"])}
    )
    print(user)
    # result = await session.execute(select(User).where(User.id == decoded_jwt["sub"]))
    # user = result.scalars().first()
    return user
