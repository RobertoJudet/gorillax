from fastapi import Depends, FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.routers import users
from app.routers import users, auth, enty
from app.db import init_db, add_admin_user, add_enty_fro_user
from motor.motor_asyncio import AsyncIOMotorClient

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8000",
    "http://localhost:3000",
    "http://127.0.0.1:8000",
    "http://127.0.0.1:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(users.router)
app.include_router(auth.router)
app.include_router(enty.router)


# @app.on_event("startup")
# async def on_startup():
#     await init_db()
#     await add_admin_user()
#     await add_enty_fro_user()


@app.on_event("startup")
async def startup_db_client():
    app.mongodb_client = AsyncIOMotorClient(
        "mongodb+srv://gorilladmin:n6ryOKSF2s83RQFl@gorillax.dwlpq8z.mongodb.net/?retryWrites=true&w=majority"
    )
    app.mongodb = app.mongodb_client["gorillax"]
    await init_db()
    # await add_admin_user()
    # await add_enty_fro_user()


@app.on_event("shutdown")
async def shutdown_db_client():
    app.mongodb_client.close()


# $ poetry run uvicorn app.main:app --reload --workers 1 --host 0.0.0.0 --port 8000^C
