import os

import bcrypt

from sqlmodel import Field, Session, SQLModel, create_engine

from sqlalchemy.future import select

from app.models.users import UserCreate, User
from app.models.enties import Enty

from app.auth.crypto import salt, hash_pass

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker


DATABASE_URL = "postgresql+asyncpg://postgres:postgres@localhost:5432/foo"

engine = create_async_engine(DATABASE_URL, echo=True, future=True)


async def init_db():
    async with engine.begin() as conn:
        print(SQLModel.metadata.tables)
        await conn.run_sync(SQLModel.metadata.drop_all)
        await conn.run_sync(SQLModel.metadata.create_all)


async def add_admin_user():
    async with AsyncSession(engine) as session:
        session.add(User(first_name="Admin", last_name="User", email="admin@admin.com", password=hash_pass("admin")))
        await session.commit()


async def add_enty_fro_user():
    async with AsyncSession(engine) as session:
        session.add(
            Enty(
                title="Matrix",
                type="movie",
                year=1999,
                user_id=1,
                cover_url="https://encrypted-tbn0.gstatic.com/images?q=tbn"
                          ":ANd9GcR3MOU9bwdtLj5bMlHioRxzkr2HEyll3bm1qab6D71WulrKJrtY",
                watched=True,
                tmdb_id=121,
                availability="info",
            )
        )
        session.add(
            Enty(
                title="Friends",
                type="tv series",
                year=1994,
                user_id=1,
                cover_url="url",
                watched=True,
                tmdb_id=1668,
                availability="info",
            )
        )
        session.add(
            Enty(
                title="Titanic",
                type="movie",
                year=1997,
                user_id=1,
                cover_url="https://upload.wikimedia.org/wikipedia/en/1/18/Titanic_%281997_film%29_poster.png",
                watched=False,
                tmdb_id=945657,
                availability="info",
            )
        )
        await session.commit()


async def get_session() -> AsyncSession:
    async_session = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)
    async with async_session() as session:
        yield session
