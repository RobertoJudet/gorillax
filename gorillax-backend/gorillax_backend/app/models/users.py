from typing import Optional
import uuid

from sqlmodel import SQLModel, Field
from app.models.enties import Enty

# from pydantic import SecretStr
# from pydantic import BaseModel


class UserBase(SQLModel):
    first_name: str
    last_name: str
    email: str
    planted_trees: int = 0
    # enties: list[Enty]


class User(UserBase, table=True):
    id: str = Field(default_factory=uuid.uuid4, alias="_id", primary_key=True)
    # id: int = Field(default=None, primary_key=True)
    password: str


class UserRead(UserBase):
    id: str = Field()


class UserCreate(UserBase):
    password: str
