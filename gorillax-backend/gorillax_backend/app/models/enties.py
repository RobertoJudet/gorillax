from typing import Optional, List
from enum import Enum
import uuid

# from sqlmodel import SQLModel, Field
from pydantic import BaseModel, Field


class EntyTypes(str, Enum):
    movie = "movie"
    tv_series = "tv series"
    video_game = "video game"
    book = "book"
    episode = "episode"

    @classmethod
    def list(cls):
        return list(map(lambda x: x.value, cls))


class EntyBase(BaseModel):
    title: str
    type: EntyTypes = Field(default=EntyTypes.movie)
    year: int
    cover_url: str
    tmdb_id: int = Field(default=0)
    availability: str


class Enty(EntyBase):
    # id: str = Field(default_factory=uuid.uuid4, alias="_id")
    id: int = Field(default=None)
    watched: bool
