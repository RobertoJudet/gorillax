from typing import Optional

from sqlmodel import SQLModel, Field


class AuthBase(SQLModel):
    jwt_token: str


class Auth(AuthBase, table=True):
    id: str = Field(default=None, primary_key=True)
    user_id: Optional[str] = Field(default=None)



class AuthRead(AuthBase):
    pass


class Login(SQLModel):
    email: str
    password: str
